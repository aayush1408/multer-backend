const mongoose = require('mongoose');

const ImageSchema = mongoose.Schema({
    imageData: {
        type: Buffer
    }
});

const Image = module.exports = mongoose.model('Image', ImageSchema);