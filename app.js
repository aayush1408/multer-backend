const express = require('express');
const multer = require('multer');
const path = require('path');
const ejs = require('ejs');
const mongoose = require('mongoose');
const Image = require('./models/image');
const fs = require('fs');
mongoose.connect('mongodb://localhost:27017/imageUpload');
//Set storage engine
const storage = multer.diskStorage({
    destination: './public/uploads',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
//Init upload
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
    fileFilter: (req, file, cb) => {
        checkFileType(file, cb);
    }
}).single('myImage');


//Checkfile type
function checkFileType(file, cb) {
    const filetypes = /jpeg|png|jpg|gif/;
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    const mimetype = filetypes.test(file.mimetype);
    if (mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error :Images only')
    }
}

const app = express();

//EJS
app.set('view engine', 'ejs');

//public
app.use(express.static('./public'));

app.get('/', (req, res) => {
    res.render('index');
});
app.post('/upload', (req, res) => {
    upload(req, res, (err) => {
        if (err) {
            res.json({ success: false, msg: err });
        }
        else {
            if (req.file == undefined) {
                res.json({ suceess: true, msg: 'No file selected' });
            } else {
                console.log(req.file.path);
                var newImage = new Image({ imageData: fs.readFileSync(req.file.path) });
                newImage.save((err) => {
                    if (!err) {
                        res.json({ success: true, msg: 'Image uploaded' });
                    }
                });
            }
        }
    })
});

app.get('/api/images', (req, res) => {
    Image.find().exec(function (err, image) {
        if (err) throw err;
        else {
            res.json(image.imageData);
            // var imageString = Buffer.from(image[0].data.data, '7bit').toString('ascii')
            // res.send(imageString);
        }
    });
})

app.listen(4000, () => {
    console.log('server running on port 3000');
});
